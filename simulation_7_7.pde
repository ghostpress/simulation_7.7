float electricField = 5.0;
float magneticField = 7.0;
float mass = 1.2;
float q = 1.0;
float w = ( magneticField * q) / mass; 
float initialVelocity = 5.8;
float t;
float deltaT = 0.0001;

float convertScreenToWorld = 1E-3; // 1E-3 px = 1m

void setup() {
  size( 600, 600 );
  background( 0, 2, 50 );
}

void draw() {
  stroke( 255 );
  strokeWeight( 5 );
  
  translate( width/2, height/2 );
  point( xPositionScreen( xPositionWorld( t ) ), yPositionScreen( yPositionWorld( t ) ) );
  
  t += deltaT; 
}


public float xPositionWorld( float t ) {
  float k = ( mass * ( ( magneticField * initialVelocity ) - electricField ) ) / ( q * magneticField * magneticField); // delete later
  return k * ( 1 - cos( w * t ) );  
}

public float yPositionWorld( float t ) {
  float k = ( mass * ( ( magneticField * initialVelocity ) - electricField ) ) / ( q * magneticField * magneticField);
  return ( k * sin( w * t ) ) + ( ( electricField / magneticField ) * t );
}


public float xPositionScreen( float xInWorld ) {
  return xInWorld / convertScreenToWorld;
}

public float yPositionScreen( float yInWorld ) {
  return yInWorld / convertScreenToWorld;
}